package reporter

import (
	"context"
	"fmt"
	"log"
	"strconv"

	"bitbucket.org/smeshkow/weather-reporter/client"
)

const (
	openURLTemplate = "http://api.openweathermap.org/data/2.5/weather?q=%s&units=metric&appid=2326504fb9b100bee21400190e4dbe6d"
)

// OpenReporter ...
type OpenReporter struct {
	Client *client.NetClient
}

// openReport is the result of weather query with YQL from the Yahoo API
//
// example of returned JSON payload:
// {
// 	"coord": {
// 	  "lon": 151.22,
// 	  "lat": -33.85
// 	},
// 	"weather": [
// 	  {
// 		"id": 520,
// 		"main": "Rain",
// 		"description": "light intensity shower rain",
// 		"icon": "09n"
// 	  }
// 	],
// 	"base": "stations",
// 	"main": {
// 	  "temp": 17,
// 	  "pressure": 1026,
// 	  "humidity": 63,
// 	  "temp_min": 17,
// 	  "temp_max": 17
// 	},
// 	"visibility": 10000,
// 	"wind": {
// 	  "speed": 10.3,
// 	  "deg": 180,
// 	  "gust": 15.4
// 	},
// 	"clouds": {
// 	  "all": 75
// 	},
// 	"dt": 1537695000,
// 	"sys": {
// 	  "type": 1,
// 	  "id": 8233,
// 	  "message": 0.0036,
// 	  "country": "AU",
// 	  "sunrise": 1537645406,
// 	  "sunset": 1537689128
// 	},
// 	"id": 2147714,
// 	"name": "Sydney",
// 	"cod": 200
//   }
type openReport struct {
	Main *main     `json:"main"`
	Wind *openWind `json:"wind"`
}

type main struct {
	Temp string `json:"temp"`
}

type openWind struct {
	Speed string `json:"speed"`
}

// GetReport queries OpenWeatherMap report for given city.
func (r *OpenReporter) GetReport(ctx context.Context, city string) (*WeatherResponse, error) {
	locationURL := fmt.Sprintf(openURLTemplate, city)

	log.Printf("GET OpenWeatherMap: \"%s\"", locationURL)

	var response yahooReport
	err := r.Client.Get(ctx, locationURL, &response)
	if err != nil {
		return nil, fmt.Errorf("error in getting OpenWeatherMap report: %v", err)
	}

	windFloat, err := strconv.ParseFloat(response.Query.Results.Channel.Wind.Speed, 64)
	if err != nil {
		return nil, fmt.Errorf("error in parsing OpenWeatherMap wind speed: %v", err)
	}

	tempFloat, err := strconv.ParseFloat(response.Query.Results.Channel.Item.Condition.Temp, 64)
	if err != nil {
		return nil, fmt.Errorf("error in parsing OpenWeatherMap temperature: %v", err)
	}

	return &WeatherResponse{
		WindSpeed:          int(windFloat),
		TemperatureDegrees: int(tempFloat),
	}, nil
}
