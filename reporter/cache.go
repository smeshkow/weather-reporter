package reporter

import (
	"sync"
)

type entry struct {
	resp       *WeatherResponse
	lastAccess int64
}

type cache interface {
	get(key string) *entry
	put(key string, response *WeatherResponse)
}

type responseCache struct {
	store sync.Map
}

// newResponseCache creates new instance of response cache.
func newResponseCache() *responseCache {
	return &responseCache{}
}

// Get returns stored value.
func (c *responseCache) get(key string) *entry {
	e, ok := c.store.Load(key)
	if ok {
		return e.(*entry)
	}
	return nil
}

// Put stores new response into cache and updates lastAccess time.
func (c *responseCache) put(key string, response *WeatherResponse) {
	e := &entry{
		resp:       response,
		lastAccess: timeNow().Unix(),
	}
	c.store.Store(key, e)
}
