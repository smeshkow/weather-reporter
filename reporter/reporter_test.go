package reporter

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

type faultyReporter struct {
	count int
}

func (r *faultyReporter) GetReport(ctx context.Context, city string) (*WeatherResponse, error) {
	r.count++
	return nil, errors.New("fail")
}

type goodReporter struct {
	count int
}

func (r *goodReporter) GetReport(ctx context.Context, city string) (*WeatherResponse, error) {
	r.count++
	return &WeatherResponse{WindSpeed: 2, TemperatureDegrees: 2}, nil
}

func Test_ChainReporter_fallsbackToNext(t *testing.T) {
	gr := &goodReporter{}
	fr := &faultyReporter{}
	chain := []Reporter{fr, gr}

	reporter := NewChainReporter(10, chain)

	resp, err := reporter.GetReport(context.TODO(), "Sydney")

	assert.Equal(t, 1, fr.count)
	assert.Equal(t, 1, gr.count)
	assert.Nil(t, err)
	assert.NotNil(t, resp)
}
