package reporter

import (
	"context"
	"fmt"
	"log"
	"net/url"
	"strconv"
	"time"

	"bitbucket.org/smeshkow/weather-reporter/client"
)

const (
	yahooQueryTemplate = "select item.condition, wind from weather.forecast where woeid in (select woeid from geo.places(1) where text='%s') and u='c'"
	yahooURLTemplate   = "http://query.yahooapis.com/v1/public/yql?q=%s&format=json"
)

// YahooReporter ...
type YahooReporter struct {
	Client *client.NetClient
}

// yahooReport is the result of weather query with YQL from the Yahoo API
//
// example of returned JSON payload:
//
// {
//  "query": {
// 	"count": 1,
// 	"created": "2018-09-22T23:13:11Z",
// 	"lang": "en-US",
// 	"results": {
// 	 "channel": {
// 	  "wind": {
// 	   "chill": "59",
// 	   "direction": "225",
// 	   "speed": "17.70"
// 	  },
// 	  "item": {
// 	   "condition": {
// 		"code": "34",
// 		"date": "Sun, 23 Sep 2018 08:00 AM AEST",
// 		"temp": "15",
// 		"text": "Mostly Sunny"
// 	   }
// 	  }
// 	 }
// 	}
// }
type yahooReport struct {
	Query *query `json:"query"`
}

type query struct {
	Created time.Time `json:"created"`
	Results *results  `json:"results"`
}

type results struct {
	Channel *channel `json:"channel"`
}

type channel struct {
	Wind *wind `json:"wind"`
	Item *item `json:"item"`
}

type wind struct {
	Speed string `json:"speed"`
}

type item struct {
	Condition *condition `json:"condition"`
}

type condition struct {
	Temp string `json:"temp"`
}

// GetReport queries Yahoo Weather report for given city.
func (r *YahooReporter) GetReport(ctx context.Context, city string) (*WeatherResponse, error) {
	escapedQuery := url.QueryEscape(fmt.Sprintf(yahooQueryTemplate, city))
	locationURL := fmt.Sprintf(yahooURLTemplate, escapedQuery)

	log.Printf("GET Yahoo: \"%s\"", locationURL)

	var response yahooReport
	err := r.Client.Get(ctx, locationURL, &response)
	if err != nil {
		return nil, fmt.Errorf("error in getting Yahoo report: %v", err)
	}

	windFloat, err := strconv.ParseFloat(response.Query.Results.Channel.Wind.Speed, 64)
	if err != nil {
		return nil, fmt.Errorf("error in parsing Yahoo wind speed: %v", err)
	}

	tempFloat, err := strconv.ParseFloat(response.Query.Results.Channel.Item.Condition.Temp, 64)
	if err != nil {
		return nil, fmt.Errorf("error in parsing Yahoo temperature: %v", err)
	}

	return &WeatherResponse{
		WindSpeed:          int(windFloat),
		TemperatureDegrees: int(tempFloat),
	}, nil
}
