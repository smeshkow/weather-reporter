package reporter

import (
	"context"
	"log"
)

// Reporter ...
type Reporter interface {
	GetReport(ctx context.Context, city string) (*WeatherResponse, error)
}

// WeatherResponse ...
type WeatherResponse struct {
	WindSpeed          int `json:"wind_speed"`
	TemperatureDegrees int `json:"temperature_degrees"`
}

// ChainReporter retrieves
type ChainReporter struct {
	cacheTTL int // time to consider cache to be stale
	cache    *responseCache
	chain    []Reporter
}

// NewChainReporter returns new instance of chained weather reporter with given TTL in seconds.
func NewChainReporter(cacheTTL int, chain []Reporter) *ChainReporter {
	cache := newResponseCache()
	return &ChainReporter{
		cacheTTL: cacheTTL,
		cache:    cache,
		chain:    chain,
	}
}

// GetReport returns weather report from cache (if in boundaties of cache's TTL in seconds)
// or from chain of reporters, falls back to cache if every reporter in the chain failed.
func (wr *ChainReporter) GetReport(ctx context.Context, city string) (resp *WeatherResponse, err error) {
	entry := wr.cache.get(city)
	if entry != nil {
		log.Printf("found entry in cache for key \"%s\"", city)

		// if cache has entry for the city and it is not stale, then return it
		if timeNow().Unix()-entry.lastAccess < int64(wr.cacheTTL) {
			log.Println("returning value from cache")
			resp = entry.resp
			return
		}
	}

	// go through the chain of repoters
	for i, r := range wr.chain {
		resp, err = r.GetReport(ctx, city)
		if err == nil {
			// success -> update cache
			log.Printf("fetched %s with reporter #%d in chain", city, i+1)
			wr.cache.put(city, resp)
			log.Printf("updated cache entry for key \"%s\"", city)
			return
		}
		log.Printf("error in reporter: %v", err)
	}

	return
}
