package reporter

import (
	"time"
)

// timeNow returns current time in UTC.
func timeNow() time.Time {
	return time.Now().UTC()
}
