package config

import (
	"io/ioutil"
	"time"

	"gopkg.in/yaml.v2"
)

// Load loads configuration from file.
func Load(file string) (cfg Config, err error) {
	cfg.Server.Addr = ":8080"
	cfg.Server.ReadTimeout = 5 * time.Second
	cfg.Server.WriteTimeout = 5 * time.Second
	cfg.Server.IdleTimeout = 5 * time.Second

	data, err := ioutil.ReadFile(file)
	if err != nil {
		return
	}

	if err = yaml.Unmarshal(data, &cfg); err != nil {
		return
	}

	return
}

// Config represent configuratoion of application.
type Config struct {
	Server struct {
		Addr         string
		ReadTimeout  time.Duration
		WriteTimeout time.Duration
		IdleTimeout  time.Duration
	}
	Reporter struct {
		DefaultCity       string
		CacheTTLInSeconds int
	}
	Processor struct {
		RequestRatePerSecond    float64
		RequestTimeoutInSeconds int
	}
}
