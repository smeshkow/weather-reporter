package client

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

// NetClient ...
type NetClient struct {
	client *http.Client
}

// NewNetClient ...
func NewNetClient() *NetClient {
	return &NetClient{
		client: &http.Client{
			Timeout: time.Second * 10,
		},
	}
}

// Get performs an HTTP GET request to provided URL,
// on success writes to provided response object.
func (c *NetClient) Get(ctx context.Context, url string, response interface{}) error {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return err
	}

	resp, err := c.client.Do(req.WithContext(ctx))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected reponse status: %s", resp.Status)
	}

	err = readResponse(resp.Body, response)
	if err != nil {
		return err
	}

	return nil
}

func readResponse(reader io.Reader, response interface{}) error {
	err := json.NewDecoder(reader).Decode(response)
	if err != nil {
		return err
	}
	return nil
}
