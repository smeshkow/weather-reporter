# Weather Reporter

Weather Reporter - is a web app for serving weather reports.

Reports are served in following JSON format:

```json
{
 "wind_speed": 20,
 "temperature_degrees": 29
}
```

App uses [Gorilla Mux](https://github.com/gorilla/mux) for routing.

Main method is in `app.go`, it is responsible for loading configuration file `_resources/config.yml`, registering handlers via `#RegisterHandlers` and starting server on port `:8080`.

There are only two routes in the server:

* GET `/version` - returns version of the app;
* GET `/v1/weather` - returns weather report in JSON format;

`Processor` (`/app/processor.go`) is responsible for providing weather report in `#weatherHandler`, `weatherProcessor` allows up to 1000 un-blocked requests, after amount of requests reaches 1000 clients will have to wait. `WeatherProcessor` has rate limiting in place, so it allows only 2 requests per second. All requests from `Processor` are delegated to `Reporter`. `Reporter` (`/reporter/reporter.go`) is an interface that allows expansion of weather reports to various weather providers.

Main implementation of the `Reporter` is `ChainReporter`, which incapsulates slice of reporters, to allow ordered invocation (based on priority) of chained reporters. `ChainReporter` contains two reporters at the moment: `yahooReporter` - `/reporter/yahoo_reporter.go` (for making calls to [Yahoo](https://developer.yahoo.com/weather/)) and `openReporter` - `/reporter/open_reporter.go` (for making calls to [OpenWeatherMap](https://openweathermap.org/current)).

`ChainReporter` relies on the `responseCache` (`/reporter/cache.go`) to prvode `WeatherResponse`, `responseCache` serves as a fallback mechanism if none of the reporters is successful and also as a performance improvement - to serve value from cache if is not older than 3 seconds.


## Sacrifices

#### Tests
It is not TDD at all, obviously I've sacrificed pretty much all the testing in favour of delivering task, due to the lack of free time.

#### Too specific
There are some places where the code is quite speciic to the given problem, for example `weatherProcessor` it only works with `WeatherResponse`, it could have been generalised, but I've decided not to bother.

#### Documentation

You want see much of the GoDocs in code or comments, and those that you'll see might not be the best.

#### Logging & Metrics

Logging is pretty poor, I could have done a better job with having log levels (e.g. with [zerolog](https://github.com/rs/zerolog)). No metrics at all.

#### External cache in Redis to have nodes in sync

Nope.

#### Cross node sync for rate limiting

Nope.

#### Load testing

Obviously no load testing at all :)

## Startup

There are several ways to spinup server locally. 
After server is started, use curl: 
```bash
curl -X "GET" "http://localhost:8080/v1/weather?city=$CITY"
```
or script:
```bash
./_bin/get-report.sh $CITY
```

### Go tool

Requires Go to be installed. Run  to start local server `go run app.go`.

### Dockerfile

1st build with `docker build . -t weather_reporter -f ./Dockerfile`, then run with `docker run -p 8080:8080 weather_reporter`.

### docker-compose

To build and run server use `docker-compose run -p 8080:8080 webapp`

### make

See `Makefile`: 

* `make run` - is  just a wrapper around `go run app.go`.
* `make serve` - is a wrapper for docker-compose startup.
