#! /bin/bash

CITY="$1"

if [ -z "$CITY" ]; then
  echo "Setting \$CITY to sydney"
  CITY="sydney"
fi

curl -X "GET" "http://localhost:8080/v1/weather?city=$CITY" | jq