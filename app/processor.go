package app

import (
	"context"
	"errors"
	"log"
	"time"

	"bitbucket.org/smeshkow/weather-reporter/reporter"
)

// weatherRequest ...
type weatherRequest struct {
	city   string
	respCh chan *reporter.WeatherResponse
	ctx    context.Context
}

// weatherProcessor ...
type weatherProcessor struct {
	rate     time.Duration
	timeout  int
	requests chan *weatherRequest
	reporter reporter.Reporter
}

// newProcessor creates new instance of Processor,
// which is ready to process incoming requests.
func newProcessor(reporter reporter.Reporter, rps float64, timeout int) *weatherProcessor {
	rate := int(float64(time.Second) / rps)
	processor := &weatherProcessor{
		rate:     time.Duration(rate), // e.g. 2 RPS to reporter
		timeout:  timeout,
		requests: make(chan *weatherRequest, 1000), // Allow up to 1000 un-blocked requests
		reporter: reporter,
	}

	// Start processing requests in separate routine
	go processor.run()

	return processor
}

// run starts processing of incoming requests.
func (wp *weatherProcessor) run() {
	limiter := time.Tick(wp.rate)
	for req := range wp.requests {
		<-limiter
		// do not wait for response synchronously
		// in order to not slow down rate
		go wp.fulfillRequest(req)
	}
}

// fulfillRequest fulfills request or aborts call if request context was cacnelled.
func (wp *weatherProcessor) fulfillRequest(req *weatherRequest) {
	resp, err := wp.reporter.GetReport(req.ctx, req.city)
	if err != nil {
		log.Printf("error in getting weather report: %v", err)
		return
	}

	select {
	case <-req.ctx.Done():
		return // context aborted, don't send response
	default:
		req.respCh <- resp // context is alive, submit response
	}
}

// getWeatherReport retrieves weather report.
func (wp *weatherProcessor) getWeatherReport(ctx context.Context, city string) (resp *reporter.WeatherResponse, err error) {
	ch := make(chan *reporter.WeatherResponse)
	newCtx, cancel := context.WithCancel(ctx)
	defer cancel()
	wp.requests <- &weatherRequest{city: city, respCh: ch, ctx: newCtx}
	defer close(ch)
	select {
	case r := <-ch:
		resp = r
	case <-time.After(time.Second * time.Duration(wp.timeout)):
		err = errors.New("timeout waiting")
	}
	return
}
