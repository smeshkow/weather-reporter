package app

import (
	"context"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"

	"bitbucket.org/smeshkow/weather-reporter/reporter"
)

type countingReporter struct {
	count int
	*sync.WaitGroup
}

func (r *countingReporter) GetReport(ctx context.Context, city string) (*reporter.WeatherResponse, error) {
	r.count++
	r.WaitGroup.Done()
	return &reporter.WeatherResponse{}, nil
}

func Test_Processor_Fullfills(t *testing.T) {
	wg := sync.WaitGroup{}
	wg.Add(3)

	reporter := &countingReporter{WaitGroup: &wg}
	processor := newProcessor(reporter, 2.0, 5)

	processor.getWeatherReport(context.TODO(), "Sydney")
	processor.getWeatherReport(context.TODO(), "Sydney")
	processor.getWeatherReport(context.TODO(), "Sydney")

	wg.Wait()

	assert.Equal(t, 3, reporter.count)
}
