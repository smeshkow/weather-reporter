package app

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"

	"bitbucket.org/smeshkow/weather-reporter/client"
	"bitbucket.org/smeshkow/weather-reporter/config"
	"bitbucket.org/smeshkow/weather-reporter/reporter"
)

var (
	version     string
	defaultCity string
)

// RegisterHandlers registers all handlers of the application.
func RegisterHandlers(appVersion string, cfg *config.Config) http.Handler {
	version = appVersion
	defaultCity = cfg.Reporter.DefaultCity

	netClient := client.NewNetClient()

	// chain of processors - if 1st processor fails,
	// then next in the chain is being called.
	chain := []reporter.Reporter{
		&reporter.YahooReporter{Client: netClient},
		&reporter.OpenReporter{Client: netClient},
	}

	reporter := reporter.NewChainReporter(cfg.Reporter.CacheTTLInSeconds, chain)
	processor := newProcessor(reporter, cfg.Processor.RequestRatePerSecond, cfg.Processor.RequestTimeoutInSeconds)

	// Use gorilla/mux for rich routing.
	// See http://www.gorillatoolkit.org/pkg/mux
	r := mux.NewRouter()

	// Shows current version of the App
	r.Methods(http.MethodGet).Path("/version").Handler(appHandler(versionHandler))

	// API V1
	apiV1 := r.PathPrefix("/v1").Subrouter()
	apiV1.Methods(http.MethodGet).Path("/weather").
		Handler(appHandler(weatherHandler(processor)))

	return r
}

// GET /version
func versionHandler(rw http.ResponseWriter, req *http.Request) *appError {
	return writeResponse(rw, map[string]interface{}{
		"version": version,
	})
}

// GET /v1/weather?city=<city> provides weather report in JSON format.
func weatherHandler(processor *weatherProcessor) func(rw http.ResponseWriter, req *http.Request) *appError {
	return func(w http.ResponseWriter, r *http.Request) *appError {
		city := r.FormValue("city")
		if len(city) == 0 {
			city = defaultCity
		}

		resp, err := processor.getWeatherReport(r.Context(), city)
		if err != nil {
			return &appError{
				Error:   err,
				Message: "error in getting report",
				Code:    500,
			}
		}

		return writeResponse(w, resp)
	}
}

// http://blog.golang.org/error-handling-and-go
type appHandler func(http.ResponseWriter, *http.Request) *appError

type appError struct {
	Error   error
	Message string
	Code    int
}

func (fn appHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if e := fn(w, r); e != nil { // e is *appError, not os.Error.
		log.Printf("Handler error: status code: %d, message: %s, underlying err: %#v",
			e.Code, e.Message, e.Error)

		http.Error(w, e.Message, e.Code)
	}
}

func appErrorf(err error, format string, v ...interface{}) *appError {
	return &appError{
		Error:   err,
		Message: fmt.Sprintf(format, v...),
		Code:    500,
	}
}

// writeResponse writes response to provided ResponseWriter in JSON format.
func writeResponse(rw http.ResponseWriter, response interface{}) *appError {
	err := json.NewEncoder(rw).Encode(response)
	if err != nil {
		return &appError{
			Error:   err,
			Message: fmt.Sprintf("error in response write: %v", err),
			Code:    http.StatusInternalServerError,
		}
	}
	rw.Header().Set("Content-Type", "application/json")
	return nil
}
