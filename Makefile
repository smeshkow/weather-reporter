.PHONY: install lint test build run serve clean

TAG?=$(shell git rev-list HEAD --max-count=1 --abbrev-commit)
APP_NAME?=weather_reporter

export TAG
export APP_NAME

all: lint test

install:
	go get .

lint: install
	go get -u github.com/alecthomas/gometalinter
	gometalinter --install
	gometalinter --fast --vendor ./...

test:
	go test -v ./...

build:
	env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-X main.version=$(TAG)" -o $(APP_NAME) .

run:
	go run app.go

serve: build
	docker-compose build --no-cache webapp
	docker-compose run -p 8080:8080 webapp

clean:
	rm ./$(APP_NAME)
