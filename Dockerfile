FROM alpine:3.4

RUN apk -U add ca-certificates

EXPOSE 8080

ADD weather_reporter /bin/weather_reporter
ADD _resources/config.yml /etc/weather_reporter/config/config.yml

CMD ["weather_reporter", "-config", "/etc/weather_reporter/config/config.yml"]
