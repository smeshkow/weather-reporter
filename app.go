package main

import (
	"flag"
	"log"
	"net/http"

	"bitbucket.org/smeshkow/weather-reporter/app"
	"bitbucket.org/smeshkow/weather-reporter/config"
)

var (
	version = "untagged"
	cfg     config.Config
)

func main() {
	filename := flag.String("config", "_resources/config.yml", "Configuration file")
	flag.Parse()

	var err error
	cfg, err = config.Load(*filename)
	if err != nil {
		log.Fatalf("failed to load configuration %s: %v", *filename, err)
	}

	srv := &http.Server{
		ReadHeaderTimeout: cfg.Server.ReadTimeout,
		IdleTimeout:       cfg.Server.IdleTimeout,
		ReadTimeout:       cfg.Server.ReadTimeout,
		WriteTimeout:      cfg.Server.WriteTimeout,
		Addr:              cfg.Server.Addr,
		Handler:           app.RegisterHandlers(version, &cfg),
	}

	log.Printf("starting app on: %s\n", cfg.Server.Addr)
	if err = srv.ListenAndServe(); err != nil {
		log.Fatalf("failed to start server: %v", err)
	}
}
